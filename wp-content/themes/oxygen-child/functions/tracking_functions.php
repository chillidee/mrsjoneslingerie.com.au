<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


add_action('wc_shipment_tracking_get_providers', 'wc_shipment_tracking_add_custom_provider');

function wc_shipment_tracking_add_custom_provider($providers) {

    $providers['Australia']['Star Track'] = 'https://startrack.com.au/?id=%1$s';

    return $providers;
}

add_filter('woocommerce_shipment_tracking_default_provider', 'custom_woocommerce_shipment_tracking_default_provider');

function custom_woocommerce_shipment_tracking_default_provider($provider) {
    $provider = 'Star Track';
    return $provider;
}

add_action('rest_api_init', function () {
    register_rest_route('tracking_api', 'new_tracking_code', array(
        'methods' => 'POST',
        'callback' => 'tracking_new_tracking_code'
    ));
});

function tracking_new_tracking_code() {

}
