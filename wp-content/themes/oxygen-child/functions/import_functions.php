<?php
add_action('wp_ajax_import_live_products', 'import_live_products_callback');
add_action('wp_ajax_import_new_product', 'import_new_product_callback');

add_filter('woocommerce_show_variation_price', function () {
    return TRUE;
});

function get_live_site_url() {
    return 'https://www.meandmrsjones.com.au';
}

function import_live_products_callback() {
    set_time_limit(0);
    echo curl_url(get_live_site_url() . '/wp-json/admin_api/get_new_products', array('api_key' => X_SITES_API_KEY, 'cat_id' => $_POST['cat_id'], 'imported_products' => json_encode(get_imported_live_products())));
    wp_die();
}

function prepare_products_for_import($products) {
    foreach (json_decode($products, true) as $product):
        add_live_product(get_product_meta_from_live_site($product));
        break;
    endforeach;
}

function get_imported_live_products() {
    global $wpdb;
    $cleanArray = array();
    $products = $wpdb->get_results("SELECT wp_postmeta.meta_value FROM wp_posts, wp_postmeta WHERE wp_posts.post_type = 'product' AND wp_posts.ID = wp_postmeta.post_id AND wp_postmeta.meta_key = '_mmj_product_id'");
    foreach ($products as $product):
        if (!in_array($product->meta_value, $cleanArray)):
            array_push($cleanArray, $product->meta_value);
        endif;
    endforeach;
    return $cleanArray;
}

function get_product_meta_from_live_site($product_id) {
    return json_decode(curl_url(get_live_site_url() . '/get_product_meta', array('api_key' => X_SITES_API_KEY, 'product_id' => $product_id)), true);
}

add_action('admin_footer', function () {
    wp_enqueue_script('admin_import_products', '/wp-content/themes/oxygen-child/js/admin_import_products.js');
});

add_action('admin_menu', 'import_live_products_menu');

function import_live_products_menu() {
    add_options_page('Import Live Products', 'Import Live Products', 'manage_options', 'import-live-products', 'import_live_products_menu_options');
}

function import_live_products_menu_options() {
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    html_content();
}

function html_content() {
    ?>
    <div class="wrap">
        <h2>Import Products</h2>
        <?php
        if (isset($_POST['check_stock'])):
            $results = checkstock();
            ?>
            <div class="results-container updated">
                <p>
                    Number of products in stock: <?php echo $results["in_stock"] ?>.
                </p>
                <p>
                    Number of products out of stock: <?php echo $results["no_stock"] ?>.
                </p>
            </div>
        <?php endif; ?>
        <div class="progress hidden">
            <div class="progress-bar progress-bar-striped active" role="progressbar"
                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                0%
            </div>
        </div>
        <p class="progress-report hidden">Connecting to <?php echo get_live_site_url() ?>.</p>
        <div class="form-group">
            <label>Category ID</label>
            <input name="import_from_category"/>
            <button name="import_products" type="button" class="button-primary">Import Products</button>
        </div>
        <form method="POST">
            <button value="bunch_products" name="bunch_products" class="button-primary" style="margin: 20px 0">Bunch
                Products
            </button>
        </form>
        <form method="POST">
            <button value="reset_prices" name="reset_prices" class="button-primary" style="margin: 20px 0">Reset
                Prices
            </button>
        </form>

        <form method="POST">
            <button value="check_stock" name="check_stock" class="button-primary" style="margin: 20px 0">Check Stock
            </button>
        </form>
        <form method="POST">
            <button value="reset_guid" name="reset_guid" class="button-primary" style="margin: 20px 0">Reset GUID
            </button>
        </form>
    </div>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?php
    if (isset($_POST['bunch_products'])):
        setVarableProducts(88);
    endif;
    if (isset($_POST['reset_prices'])):
        reset_prices();
    endif;

    if (isset($_POST['reset_guid'])):
        changeGUID();
    endif;
}

function changeGUID() {
    global $wpdb;
    $posts = $wpdb->get_results("SELECT * FROM wp_posts");
    foreach ($posts as $post):
        $guid = $post->guid;
        $id = (int) $post->ID;
        $guid = str_replace("http://staging.", "https://www.", $guid);
        $guid = str_replace("http://www.", "https://www.", $guid);
        $guid = str_replace("http://mrsjoneslingerie", "https://www.mrsjoneslingerie", $guid);
        var_dump($wpdb->query("UPDATE wp_posts SET `guid` = '$guid' WHERE `ID` = $id"));
    endforeach;
}

function reset_prices() {
    ini_set('memory_limit', '512M');
    set_time_limit(0);
    global $wpdb;
    foreach ($wpdb->get_results("SELECT ID FROM wp_posts WHERE post_status = 'publish' AND post_type = 'product'") as $_product):
        $_wholesalePrice = (float) get_post_meta($_product->ID, '_wholesale_price', true);
        if ($_wholesalePrice != 0):
            $_markup = ($_wholesalePrice / 100) * 80;
            $_newPrice = number_format($_wholesalePrice + $_markup);
            update_post_meta($_product->ID, '_regular_price', $_newPrice);
            update_post_meta($_product->ID, '_price', $_newPrice);
            foreach ($wpdb->get_results("SELECT ID FROM wp_posts WHERE post_parent = $_product->ID") as $_child_product):
                update_post_meta($_child_product->ID, '_regular_price', $_newPrice);
                update_post_meta($_child_product->ID, '_price', $_newPrice);
            endforeach;
        endif;
        wc_delete_product_transients($_product->ID);
    endforeach;
}

function curl_url($url, $fields) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);

    if (isset($fields) && !empty($fields)):
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    endif;

    $result = curl_exec($ch);

    $info = curl_getinfo($ch);

    curl_close($ch);

    return ($info['http_code'] == 200) ? $result : $info['http_code'];
}

function import_new_product_callback() {
    $live_id = htmlentities($_POST['live_id']);
    $productInfo = curl_url(get_live_site_url() . '/wp-json/admin_api/get_product_meta', array('api_key' => X_SITES_API_KEY, 'product_id' => $live_id));
    if ($productInfo != 500):
        echo new_product(json_decode($productInfo, true));
    else:
        echo false;
    endif;
    wp_die();
}

function has_been_imported($mmj_id) {
    global $wpdb;
    $result = $wpdb->get_results("SELECT post_id FROM wp_postmeta WHERE meta_key = '_mmj_product_id' AND meta_value = $mmj_id");
    return (isset($result) && !empty($result) && isset($result[0]->post_id) && !empty($result[0]->post_id)) ? $result[0]->post_id : false;
}

function new_product($product) {
    $product = $product[0];
    if (!has_been_imported($product["post"]["ID"])):
        $post_id = wp_insert_post(array(
            'post_status' => "draft",
            'post_title' => $product["post"]["post_title"],
            'post_type' => "product",
            'post_content' => $product["post"]["post_content"],
            'comment_status' => 'closed',
            'post_excerpt' => $product["post"]["post_content"]
        ));
        if ($post_id):
            $has_attachemnt = false;
            $has_gallery = false;
            foreach ($product['meta'] as $meta):
                if ($meta['meta_key'] == '_thumbnail_id'):
                    $has_attachemnt = $meta['meta_key'];
                endif;
                if ($meta['meta_key'] == '_product_image_gallery'):
                    $has_gallery = (isset($meta['meta_value']) && !empty($meta['meta_value'])) ? $meta['meta_value'] : false;
                endif;
                if ($meta['meta_key'] == '_is_child' && $meta['meta_value']):
                    $new_parent_id = get_new_parent_id($meta['meta_value']);
                    if ($new_parent_id == $meta['meta_value']):
                        $mmj_parent_id = $meta['meta_value'];
                        $new_parent_id = new_product(json_decode(curl_url(get_live_site_url() . '/wp-json/admin_api/get_product_meta', array('api_key' => X_SITES_API_KEY, 'product_id' => $mmj_parent_id)), true));
                    endif;
                    update_post_meta($post_id, '_parent_id', $mmj_parent_id);
                endif;
                update_post_meta($post_id, $meta['meta_key'], $meta['meta_value']);
            endforeach;
            update_post_meta($post_id, '_mmj_product_id', $product["post"]["ID"]);
            if (!$has_attachemnt):
                $imageLink = get_post_meta($post_id, 'main_image_link', true);
                $attch_id = doesAttachmentExsit($imageLink);
                if (!$attch_id):
                    $attch_id = create_new_attachment($imageLink);
                endif;
                update_post_meta($post_id, '_thumbnail_id', $attch_id);
            else:
                $new_attachemnt_id = create_new_attachment_exsiting($has_attachemnt);
                update_post_meta($post_id, '_thumbnail_id', $new_attachemnt_id);
                update_post_meta($new_attachemnt_id, '_mmj_thumbnail_id', $has_attachemnt);
            endif;
            if ($has_gallery):
                $gallery_string = process_gallery_attachments($has_gallery);
                if ($gallery_string):
                    update_post_meta($post_id, '_product_image_gallery', $gallery_string);
                endif;
            endif;
            return $post_id;
        else:
            return false;
        endif;
    endif;
}

function doesAttachmentExsit($imageLink) {
    global $wpdb;
    $result = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `guid` LIKE '$imageLink'");
    return (isset($result) && !empty($result) && isset($result[0]) && !empty($result[0])) ? $result[0]->ID : false;
}

function get_attachment_info($attachment_id) {
    $attachment = json_decode(curl_url(get_live_site_url() . '/wp-json/admin_api/get_attachment', array('api_key' => X_SITES_API_KEY, 'attachment_id' => $attachment_id)));
    return $attachment[0];
}

function get_new_parent_id($_mmj_product_id) {
    global $wpdb;
    $new_parent_id = $wpdb->get_results("SELECT post_id FROM `wp_postmeta` WHERE `meta_key` LIKE '_mmj_product_id' AND `meta_value` LIKE '$_mmj_product_id'");
    return (isset($new_parent_id) && !empty($new_parent_id)) ? $new_parent_id[0]->post_id : $_mmj_product_id;
}

function create_new_attachment($link) {
    ini_set('memory_limit', '512M');
    set_time_limit(0);
    $wp_upload_dir = wp_upload_dir();
    $destFolder = $wp_upload_dir['path'];
    $imageName = explode('/', $link);
    $imageName = $imageName[count($imageName) - 1];

    if (!file_exists($destFolder . $imageName)):
        get_image_from_link($link, $destFolder . $imageName);
    endif;

    $filetype = wp_check_filetype($destFolder . $imageName);

    $attachment = array(
        'guid' => $link,
        'post_mime_type' => $filetype['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', $imageName),
        'post_content' => '',
        'post_status' => 'inherit',
        'comment_status' => 'closed'
    );

    $attach_id = wp_insert_attachment($attachment, $destFolder . $imageName);

    require_once(ABSPATH . 'wp-admin/includes/image.php');

    $attach_data = wp_generate_attachment_metadata($attach_id, $destFolder . $imageName);

    wp_update_attachment_metadata($attach_id, $attach_data);

    return $attach_id;
}

function create_new_attachment_exsiting($exsiting_id) {
    $mmj_attachment = get_attachment_info($exsiting_id);
    if (isset($mmj_attachment) && !empty($mmj_attachment)):
        $does_exsit = doesAttachmentExsit($mmj_attachment->guid);
        return ($does_exsit) ? $does_exsit : create_new_attachment($mmj_attachment->guid);
    endif;
}

function process_gallery_attachments($gallery_string) {
    if (isset($gallery_string) && !empty($gallery_string)):
        $new_gallery_array = array();
        $gallery_array = explode(',', $gallery_string);
        foreach ($gallery_array as $id):
            $mmj_attachment = get_attachment_info($id);
            $does_exsit = doesAttachmentExsit($mmj_attachment->guid);
            $new_id = ($does_exsit) ? $does_exsit : create_new_attachment($mmj_attachment->guid);
            update_post_meta($new_id, '_mmj_thumbnail_id', $id);
            array_push($new_gallery_array, $new_id);
        endforeach;
    endif;
    return ($new_gallery_array) ? implode(',', $new_gallery_array) : false;
}

function get_image_from_link($url, $destFolder) {

    ini_set('allow_url_fopen', 1);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    $raw = curl_exec($ch);
    curl_close($ch);
    $fp = fopen($destFolder, 'x');
    fwrite($fp, $raw);
    fclose($fp);
}

function setVarableProducts($cat_id) {
    set_time_limit(0);
    global $wpdb;
    $size_tax = wc_attribute_taxonomy_name('Size');
    $color_tax = wc_attribute_taxonomy_name('Colours');
    $size_array = array();
    $colourArray = array();
    $results = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type = 'product' AND post_status = 'draft'");
    foreach ($results as $_product):
        if (!get_post_meta($_product->ID, '_product_variable', true) || !get_post_meta($_product->ID, '_master_variable', true)):
            $title = $_product->post_title;
            $title1 = str_replace('"', '″', $title);
            $title2 = str_replace('"', '&#8243;', $title);
            $products = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_title LIKE '$title' AND post_type = 'product' AND post_status = 'draft'");
            $products1 = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_title LIKE '$title1' AND post_type = 'product' AND post_status = 'draft'");
            $products2 = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_title LIKE '$title2' AND post_type = 'product' AND post_status = 'draft'");
            $products = array_merge($products, $products1, $products2);
            if (count($products) > 1):
                $master_id = false;
                foreach ($products as $product):
                    $_colour = get_post_meta($product->ID, 'st_colour', true);
                    $_size = get_post_meta($product->ID, 'st_size', true);
                    if (!in_array($_colour, $colourArray)):
                        array_push($colourArray, $_colour);
                    endif;
                    if (!in_array($_size, $size_array)):
                        array_push($size_array, $_size);
                    endif;
                    foreach ($results as $index => $__products):
                        if ($__products->ID == $product->ID):
                            unset($results[$index]);
                        endif;
                    endforeach;
                endforeach;
                foreach ($products as $index => $product):
                    $_colour = get_post_meta($product->ID, 'st_colour', true);
                    $_size = get_post_meta($product->ID, 'st_size', true);
                    if ($index == 0 && !get_post_meta($product->ID, '_master_variable', true)):
                        $master_id = $product->ID;
                        $attributes = array(
                            $size_tax => array(
                                'name' => $size_tax,
                                'value' => get_attribute_slug($_size, $size_tax),
                                'is_visible' => '1',
                                'is_variation' => '1',
                                'is_taxonomy' => '1'
                            ),
                            $color_tax => array(
                                'name' => $color_tax,
                                'value' => get_attribute_slug($_colour, $color_tax),
                                'is_visible' => '1',
                                'is_variation' => '1',
                                'is_taxonomy' => '1'
                            )
                        );
                        wp_set_object_terms($product->ID, $cat_id, 'product_cat', true);
                        update_post_meta($product->ID, '_product_attributes', $attributes);
                        wp_set_object_terms($product->ID, $size_array, $size_tax, true);
                        wp_set_object_terms($product->ID, $colourArray, $color_tax, true);
                        wp_set_object_terms($product->ID, 'variable', 'product_type', false);
                        update_post_meta($product->ID, '_master_variable', true);
                    else:
                        if ($master_id):
                            $guid = $wpdb->get_results("SELECT `guid` FROM `wp_posts` WHERE ID = $master_id")[0]->guid;
                            $wpdb->get_results("UPDATE `wp_posts` SET `post_title`='Product #$master_id Variation',`post_name`='product-$master_id-variation-$index',`post_parent`='$master_id',`guid`='$guid',`post_type`='product_variation' WHERE ID = $product->ID");
                            update_post_meta($product->ID, 'attribute_' . $size_tax, get_attribute_slug($_size, $size_tax));
                            update_post_meta($product->ID, 'attribute_' . $color_tax, get_attribute_slug($_colour, $color_tax));

                            update_post_meta($product->ID, '_product_variable', true);

                            wp_set_object_terms($product->ID, get_attribute_slug($_size, $size_tax), $size_tax, true);
                            wp_set_object_terms($product->ID, get_attribute_slug($_colour, $color_tax), $color_tax, true);

                            if (count($products) == 1):
                            endif;

                            WC_Product_Variable::sync($master_id);
                        endif;
                    endif;
                    $date = date("Y-m-d H:i:s");
                    wp_update_post(array('ID' => $product->ID, 'post_status' => 'publish', 'post_date' => $date, 'post_date_gmt' => $date));
                endforeach;
            else:
                $date = date("Y-m-d H:i:s");
                wp_update_post(array('ID' => $_product->ID, 'post_status' => 'publish', 'post_date' => $date, 'post_date_gmt' => $date));
                wp_set_object_terms($_product->ID, $cat_id, 'product_cat', true);
            endif;
        endif;
    endforeach;
}

function get_attribute_slug($attribute, $parent) {
    global $wpdb;
    $results = $wpdb->get_results("SELECT slug FROM `wp_terms` WHERE name = '$attribute'");
    if (empty($results)):
        wp_insert_term($attribute, $parent);
        $results = $wpdb->get_results("SELECT slug FROM `wp_terms` WHERE name = '$attribute'");
    endif;
    return (isset($results) && !empty($results)) ? $results[0]->slug : $attribute;
}

function is_product_variation($post_id) {
    global $wpdb;
    $result = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `ID` = $post_id AND `post_type` LIKE 'product_variation'");
    return (isset($result) && !empty($result)) ? true : false;
}

function pubishProducts() {
    global $wpdb;
    $products = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_status` LIKE 'draft' AND `post_type` LIKE 'product'");
    foreach ($products as $product) {
        $date = date("Y-m-d H:i:s");
        wp_update_post(array('ID' => $product->ID, 'post_status' => 'publish', 'post_date' => $date, 'post_date_gmt' => $date));
    }
}

function warehouse_db_do_query($queryString) {
    $con = warehouse_db_connect();
    $result = mysqli_query($con, $queryString);
    warehouse_db_close_db($con);
    return $result;
}

function warehouse_db_connect() {
    $connection_details = warehouse_db_get_connections_details();
    return mysqli_connect($connection_details['db_host'], $connection_details['db_username'], $connection_details['db_password'], $connection_details['db_name']);
}

function warehouse_db_get_connections_details() {
    $returnArray = array();
    $returnArray['db_name'] = "admin_feed";
    $returnArray['db_username'] = "admin_feed";
    $returnArray['db_password'] = "QL95TJbB5U";
    $returnArray['db_host'] = "45.63.31.135";
    return $returnArray;
}

function warehouse_db_close_db($db) {
    mysqli_close($db);
}

function checkstock() {
    ini_set('memory_limit', '512M');
    set_time_limit(0);
    global $wpdb;
    $results['in_stock'] = 0;
    $results['no_stock'] = 0;
    $current_products = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type = 'product' AND post_status = 'publish'");
    foreach ($current_products as $product):
        $title = $product->post_title;
        $varibleProducts = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_parent = $product->ID");
        if (count($varibleProducts) > 0):
            foreach ($varibleProducts as $_child_product):
                $colour = get_post_meta($_child_product->ID, 'attribute_pa_colours', true);
                $colour = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$colour'")[0]->name);
                $size = get_post_meta($_child_product->ID, 'attribute_pa_size', true);
                $size = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$size'")[0]->name);
                $products = warehouse_db_do_query("SELECT * FROM `feed_products` WHERE `Title` LIKE '$title' AND `Size` = '$size' AND `Colour` = '$colour'");
                if ($products->num_rows > 0):
                    $results['in_stock'] ++;
                    $products = mysqli_fetch_assoc($products);
                    update_post_meta($_child_product->ID, '_stock_status', 'instock');
                    update_post_meta($product->ID, '_stock_status', 'instock');
                    update_post_meta($product->ID, '_wholesale_price', $_product['Wholesale Price']);
                    update_post_meta($_child_product->ID, '_wholesale_price', $_product['Wholesale Price']);
                else:
                    update_post_meta($_child_product->ID, '_stock_status', 'outofstock');
                    $results['no_stock'] ++;
                endif;
            endforeach;
        else:
            $colour = get_post_meta($product->ID, 'attribute_pa_colours', true);
            $colour = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$colour'")[0]->name);
            $size = get_post_meta($product->ID, 'attribute_pa_size', true);
            $size = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$size'")[0]->name);
            $products = warehouse_db_do_query("SELECT * FROM `feed_products` WHERE `Title` LIKE '$title' AND `Size` = '$size' AND `Colour` = '$colour'");
            if ($products->num_rows > 0):
                $results['in_stock'] ++;
                $products = mysqli_fetch_assoc($products);
                update_post_meta($product->ID, '_stock_status', 'instock');
                update_post_meta($product->ID, '_wholesale_price', $_product['Wholesale Price']);
            else:
                update_post_meta($product->ID, '_stock_status', 'outofstock');
                $results['no_stock'] ++;
            endif;
        endif;
    endforeach;
    reset_prices();
    return $results;
}

function check_stock() {
    global $wpdb;
    $results = array();
    $results['in_stock'] = 0;
    $results['no_stock'] = 0;
    $current_products = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type = 'product' AND post_status = 'publish'");
    foreach ($current_products as $product):
        $title = $product->post_title;
        $varibleProducts = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_parent = $product->ID");
        $products = warehouse_db_do_query("SELECT * FROM `feed_products` WHERE `Title` LIKE '$title'");
        if (count($varibleProducts) > 0):
            foreach ($varibleProducts as $_child_product):
                $updated = false;
                $colour = get_post_meta($_child_product->ID, 'attribute_pa_colours', true);
                $colour = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$colour'")[0]->name);
                $size = get_post_meta($_child_product->ID, 'attribute_pa_size', true);
                $size = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$size'")[0]->name);
                while ($_product = mysqli_fetch_assoc($products)):
                    if (strtolower($_product['Size']) == $size && strtolower($_product['Colour']) == $colour):
                        if (get_post_meta($_child_product->ID, '_stock_status', true) == 'outofstock'):
                            update_post_meta($_child_product->ID, '_stock_status', 'instock');
                            update_post_meta($product->ID, '_stock_status', 'instock');
                            $results['in_stock'] ++;
                        endif;
                        $updated = true;
                        update_post_meta($product->ID, '_wholesale_price', $_product['Wholesale Price']);
                        update_post_meta($_child_product->ID, '_wholesale_price', $_product['Wholesale Price']);
                        break;
                    endif;
                endwhile;
                if (!$updated):
                    update_post_meta($_child_product->ID, '_stock_status', 'outofstock');
                    $results['no_stock'] ++;
                endif;
            endforeach;
        else:
            $updated = false;
            $colour = get_post_meta($product->ID, 'attribute_pa_colours', true);
            $colour = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$colour'")[0]->name);
            $size = get_post_meta($product->ID, 'attribute_pa_size', true);
            $size = strtolower($wpdb->get_results("SELECT name FROM `wp_terms` WHERE `slug` LIKE '$size'")[0]->name);
            while ($_product = mysqli_fetch_assoc($products)):
                if (strtolower($_product['Size']) == $size && strtolower($_product['Colour']) == $colour):
                    $updated = true;
                    if (get_post_meta($product->ID, '_stock_status', true) == 'outofstock'):
                        $results['in_stock'] ++;
                        update_post_meta($product->ID, '_stock_status', 'instock');
                    endif;
                    update_post_meta($product->ID, '_wholesale_price', $_product['Wholesale Price']);
                    break;
                endif;
            endwhile;
            if (!$updated):
                update_post_meta($product->ID, '_stock_status', 'outofstock');
                $results['no_stock'] ++;
            endif;
        endif;
    endforeach;
    reset_prices();
    var_dump($results);
    return $results;
}
