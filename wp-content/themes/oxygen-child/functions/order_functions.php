<?php

/*
 * -----------------------|
 *  Send order to vendors |
 * -----------------------|
 */

add_action('woocommerce_thankyou', 'mjl_send_vendor_orders');

function mjl_send_vendor_orders($order_id) {
    $order = wc_get_order($order_id);
    if ($order->post_status == "wc-processing" && !get_post_meta($order->id, "_order_sent_to_vendor", true)):
        update_post_meta($order->id, "_order_sent_to_vendor", true);
        mjl_order_created($order);
    endif;
}

function mjl_order_created($order) {
    $customer = array();
    $billingAddress = array(
        'first_name' => $order->billing_first_name,
        'last_name' => $order->billing_last_name,
        'address_1' => $order->billing_address_1,
        'address_2' => $order->billing_address_2,
        'city' => $order->billing_city,
        'state' => $order->billing_state,
        'postcode' => $order->billing_postcode,
        'country' => $order->billing_country,
        'phone' => $order->billing_phone,
        'email' => $order->billing_email
    );
    $customer['billingAddress'] = $billingAddress;
    $billingAddress = array(
        'first_name' => $order->shipping_first_name,
        'last_name' => $order->shipping_last_name,
        'address_1' => $order->shipping_address_1,
        'address_2' => $order->shipping_address_2,
        'city' => $order->shipping_city,
        'state' => $order->shipping_state,
        'postcode' => $order->shipping_postcode,
        'country' => $order->shipping_country,
        'phone' => $order->shipping_phone
    );
    $customer['shippingAddress'] = $billingAddress;
    $customer['details'] = array(
        'order_number' => $order->id
    );
    $items = $order->get_items();
    foreach ($items as $index => $item):
        $wholeseller = get_post_meta($item['product_id'], 'vendor', true);
        $customer['vendors'][$wholeseller][$item['product_id']] = $item;
    endforeach;
    mjl_sendVendorEmails($customer);
}

//function st_createOrderTextFile($vendor, $data) {
//    $file = get_template_directory() . '/orders/2417.' . $data['details']['order_number'] . '.txt';
//    $content = "001\t2417\t" . $data['shippingAddress']['first_name'] . " " . $data['shippingAddress']['last_name'] . "\tMe & Mrs Jones\tinfo@meandmrsjones.com.au\t1300 780 182\t" . $data['shippingAddress']['address_1'] . "," . $data['shippingAddress']['address_2'] . "\t" . $data['shippingAddress']['city'] . "\t" . $data['shippingAddress']['state'] . "\t" . $data['shippingAddress']['postcode'] . "\tDrop Ship\t" . $data['details']['order_number'] . "\t2417\r\n";
//    foreach ($data['vendors'] as $_vendor => $item):
//        if ($_vendor == $vendor):
//            foreach ($item as $_item):
//                $content .= "002\t" . get_post_meta($_item['product_id'], '_code', true) . "\t" . $_item['name'] . "\t\t\t" . $_item['item_meta']['_qty'][0] . "\t\r\n";
//            endforeach;
//        endif;
//    endforeach;
//    $content .= "003\tAcct\t\t\t\t\t\t\t\r\n";
//    $content .= "999\t\t\t\t\t\t\t\t";
//    file_put_contents($file, $content);
//    return $file;
//}

function mjl_createSimpleOrderTextFile($vendor, $data) {
    $vendorTrim = strtolower(str_replace(' ', '', $vendor));
    $destinationFolder = get_template_directory() . '/orders/';
    if (!file_exists($destinationFolder)) {
        mkdir($destinationFolder, 0775, true);
    }
    $file = $destinationFolder . $vendorTrim . "-" . $data['details']['order_number'] . '.txt';
    $content = "New Order From Mrs Jones Lingerie\r\n\r\n";
    $content .= "Order number " . $data['details']['order_number'] . "\r\n\r\n";
    $content .= "Customer Details\r\n\r\n";
    $content .= "Billing Address\r\n\r\n";
    $content .= "{$data['billingAddress']['first_name']} {$data['billingAddress']['last_name']}\r\n{$data['billingAddress']['email']}\r\n{$data['billingAddress']['address_1']}\r\n {$data['billingAddress']['address_2']}\r\n {$data['billingAddress']['city']}\r\n {$data['billingAddress']['state']}\r\n {$data['billingAddress']['postcode']}\r\n\r\n";
    $content .= "Shipping Address\r\n\r\n";
    $content .= "{$data['shippingAddress']['first_name']} {$data['shippingAddress']['last_name']}\r\n {$data['shippingAddress']['address_1']}\r\n {$data['shippingAddress']['address_2']}\r\n {$data['shippingAddress']['city']}\r\n {$data['shippingAddress']['state']}\r\n {$data['shippingAddress']['postcode']}\r\n\r\n";
    $content .= "Item Details\r\n\r\n";
    foreach ($data['vendors'] as $_vendor => $item):
        if ($_vendor == $vendor):
            foreach ($item as $_item):
                $_code = (isset($_item['item_meta']['_variation_id'][0]) && !empty($_item['item_meta']['_variation_id'][0])) ? get_post_meta($_item['item_meta']['_variation_id'][0], '_code', true) : get_post_meta($_item['product_id'], '_code', true);
                $_sku = (isset($_item['item_meta']['_variation_id'][0]) && !empty($_item['item_meta']['_variation_id'][0])) ? get_post_meta($_item['item_meta']['_variation_id'][0], '_barcode', true) : get_post_meta($_item['product_id'], '_barcode', true);
                $_colour = (isset($_item['item_meta']['pa_colours'][0]) && !empty($_item['item_meta']['pa_colours'][0])) ? $_item['item_meta']['pa_colours'][0] : get_post_meta($_item['product_id'], 'st_colour', true);
                $_size = (isset($_item['item_meta']['pa_size'][0]) && !empty($_item['item_meta']['pa_size'][0])) ? $_item['item_meta']['pa_size'][0] : get_post_meta($_item['product_id'], 'st_size', true);
                $content .= "Product Code: " . $_code . "\r\nProduct Name: " . $_item['name'] . "\r\nProduct Size: " . $_size . "\r\nProduct Colour: " . $_colour . "\r\nProduct Quantity: " . $_item['item_meta']['_qty'][0] . "\r\nProduct Barcode: " . $_sku . "\r\n\r\n";
            endforeach;
        endif;
    endforeach;
    file_put_contents($file, $content);
    return $file;
}

function mjl_sendVendorEmails($data) {
    $email_address = array('matt@chillidee.com.au');
    $subject = 'Mrs Jones Lingerie Order Placed';
    $headers = 'From: Mrs Jones Lingerie <info@mrsjoneslingerie.com.au>' . "\r\n";
    $file = '';
    foreach ($data['vendors'] as $vendor => $items):
        switch ($vendor):
//            case 'Windsor Wholeseller':
//                $email_address = array('weborders@windsorwholesale.com.au', 'info@meandmrsjones.com.au');
//                $file = st_createOrderTextFile($vendor, $data);
//                if (!st_ftp_windsor_order($file)):
//                    wp_mail($email_address, $subject, 'Please view attached order', $headers, $file);
//                endif;
//                break;
            case 'Wrapped Secerts':
                $email_address = array('admin@wrappedsecrets.com.au', 'des@wrappedsecrets.com.au', 'wstracking@meandmrsjones.com.au', 'orders@mrsjoneslingerie.com.au');
                $file = mjl_createSimpleOrderTextFile($vendor, $data);
                wp_mail($email_address, $subject, 'Please view attached order', $headers, $file);
                break;
            default:
                $file = st_createSimpleOrderTextFile($vendor, $data);
                wp_mail('info@meandmrsjones.co.au', 'ALERT! Order not sent to vendor', 'ALERT! Order not sent to vendor', $headers, $file);
                break;
        endswitch;
    endforeach;
}
