<?php

add_action('wp_footer', 'add_click_events');

add_filter('ninja_forms_admin_email_message_wpautop', 'wrap_email_message', 10, 1);

function wrap_email_message($message) {
    require_once PLUGINDIR . '/woocommerce/includes/class-wc-emails.php';
    $wc_email = new WC_Emails();
    return $wc_email->wrap_message('', $message);
}

function add_click_events() {
    wp_enqueue_script('click-events', get_stylesheet_directory_uri() . '/js/analytics-events.js');
}

function filter_woocommerce_terms_is_checked_default($isset) {
    return true;
}

add_filter('woocommerce_terms_is_checked_default', 'filter_woocommerce_terms_is_checked_default', 10, 1);
