<?php

/**
 * 	Oxygen WordPress Theme
 *
 * 	Laborator.co
 * 	www.laborator.co
 */
// This will enqueue style.css of child theme
add_action('wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100);

function enqueue_childtheme_scripts() {
    wp_enqueue_style('oxygen-child', get_stylesheet_directory_uri() . '/style.css');
}

require_once 'functions/import_functions.php';

require_once 'functions/order_functions.php';

require_once 'functions/tracking_functions.php';

require_once 'functions/password_functions.php';

require_once 'functions/filter_actions_functions.php';
