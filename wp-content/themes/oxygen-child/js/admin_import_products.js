jQuery(document).ready(function ($) {

    var import_button = $('button[name=import_products]');
    if (import_button.length > 0) {
        import_button.click(function () {
            var cat_id = $('input[name=import_from_category]').val();
            if (cat_id.length > 0) {
                var progess = $('.progress');
                var progress_report = $('.progress-report');
                import_button.attr('disabled', true);
                progess.removeClass('hidden');
                progress_report.removeClass('hidden');
                var data = {
                    action: 'import_live_products',
                    cat_id: cat_id
                }
                $.post(ajaxurl, data, function (response) {
                    response = JSON.parse(response);
                    var number_of_products = response.length;
                    var increasement = 100 / number_of_products;
                    var total = 0, index = 1;
                    if (number_of_products > 0) {
                        progress_report.html(number_of_products + ' new products to import');
                        var product_number = 1;
                        progress_report.html('Importing Product ' + product_number + ' of ' + number_of_products);
                        for (var i = 0; i <= number_of_products; i++) {
                            import_product(response[i], parseInt(i + '000'), function (result) {
                                console.log(result);
                                product_number++;
                                index++;
                                total += increasement;
                                change_progess_bar(total);
                                progress_report.html('Importing Product ' + product_number + ' of ' + number_of_products);
                                if (index >= number_of_products - 1) {
                                    progress_report.html('Finished importing');
                                    change_progess_bar(100);
                                    import_button.attr('disabled', false);
                                }
                            });
                        }
                    } else {
                        progress_report.html('No new products found');
                        change_progess_bar(100);
                        import_button.attr('disabled', false);
                        setTimeout(function () {
                            progess.addClass('hidden');
                            progress_report.addClass('hidden').html('');
                            change_progess_bar(0);
                        }, 5000);
                    }
                })
            } else {
                alert('Please enter a category');
            }
        })

        $('button[name=bunch_products]').click(function () {
            var cat_id = $('input[name=insert_category]');
            console.log(cat_id.length);
            if (cat_id.length > 1) {
                $('form[name=bunch_products_form]').submit();
            }
        });

    }

    function change_progess_bar(width) {
        $('.progress-bar').attr('aria-valuenow', width).css('width', width + '%').html(width.toFixed(2) + '%');
    }

    function import_product(id, timeout, callback) {
        setTimeout(function () {
            var data = {
                action: 'import_new_product',
                live_id: id
            }
            $.post(ajaxurl, data, function (result) {
                callback(result);
            });
        }, timeout);
    }


})