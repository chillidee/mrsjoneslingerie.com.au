jQuery(document).ready(function ($) {
    checkIfAnalyticsLoaded();

    if ($('.woocommerce-variation-add-to-cart').length > 0) {
        $('.woocommerce-variation-add-to-cart').click(function () {
            if ($(this).hasClass('woocommerce-variation-add-to-cart-disabled')) {
                $("<p class='woocommerce-error option-error' style='font-size: 18px;'>Please select size/colour to continue</p>").insertBefore('.variations_form')
            }
        });

        if ($('#pa_colours').length > 0) {
            $('#pa_colours').change(function () {
                if ($('.option-error').length > 0) {
                    $('.option-error').remove();
                }
            });
        }

        if ($('#pa_size').length > 0) {
            $('#pa_size').change(function () {
                if ($('.option-error').length > 0) {
                    $('.option-error').remove();
                }
            });
        }

    }

    function checkIfAnalyticsLoaded() {
        if (ga !== undefined) {
            setClickEvents();
        } else {
            setTimeout(500, checkIfAnalyticsLoaded());
        }
    }

    function setClickEvents() {
        $('button').each(function () {
            $(this).click(function () {
                ga('send', 'event', "Button Click", "Click", $(this).html());
            });
        });

        $("a").each(function () {
            $(this).click(function () {
                var label;
                if ($(this).data('ga') !== undefined) {
                    label = $(this).data('ga');
                } else {
                    label = $(this).html();
                }
                ga('send', 'event', "Link Click", "Click", label);
            });
        });
    }

})