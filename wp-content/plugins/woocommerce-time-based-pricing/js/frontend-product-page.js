var wctbp_price_update_request = null;
var wctbp_original_content = "";
jQuery(document).ready(function()
{
	jQuery(document).on('change', '.quantity input.qty', wctbp_on_quantity_change);
	jQuery(document).on('show_variation', wctbp_on_quantity_change);
});

function wctbp_on_quantity_change(event)
{
	/* console.log(event.currentTarget.value);
	console.log(jQuery('input[name=add-to-cart]').val());  */
	
	var product_id = /* jQuery('input[name=add-to-cart]').val() */ wctbp.product_id;
	var variation_id = jQuery('input[name=variation_id]').val();
	var random = Math.floor((Math.random() * 1000000) + 999);
	var formData = new FormData();
	formData.append('action', 'wctbp_update_price');
	formData.append('product_id', product_id);
	formData.append('variation_id', variation_id);
	//formData.append('quantity', event.currentTarget.value);
	formData.append('quantity', jQuery('.quantity input.qty').val());
	
	
	//UI
	//var wctbp_original_content = "";
	if(typeof variation_id !== 'undefined')
	{
		wctbp_original_content = wctbp_original_content == "" ? jQuery('div.woocommerce-variation-price span.price').html() : wctbp_original_content;
		jQuery('div.woocommerce-variation-price span.price').html(wctbp.wctbp_loading_message);
	}
	else
	{
		wctbp_original_content = wctbp_original_content == "" ? jQuery('div.summary.entry-summary p.price').html() : wctbp_original_content; //div.summary.entry-summary div p.price
		jQuery('div.summary.entry-summary p.price').html(wctbp.wctbp_loading_message);
	}
	wctbp_original_content = wctbp_original_content == wctbp.wctbp_loading_message ? "" : wctbp_original_content;
	
	if(wctbp_price_update_request != null)
		wctbp_price_update_request.abort();
	wctbp_price_update_request = jQuery.ajax({
		url: wctbp.wctbp_ajax_url+"?nocache="+random,
		type: 'POST',
		data: formData,
		async: true,
		success: function (data) 
		{
			if(data == "no")
			{
				if (typeof variation_id !== 'undefined')
					jQuery('div.woocommerce-variation-price span.price').html(wctbp_original_content);
				else
					jQuery('div.summary.entry-summary p.price').html(wctbp_original_content);
			}
			else 
			{
				if(typeof variation_id !== 'undefined')
					jQuery('div.woocommerce-variation-price span.price').html(data);
				else
					jQuery('div.summary.entry-summary p.price').html(data);
			}
						
		},
		error: function (data) 
		{
			//console.log(data);
			//alert("Error: "+data);
		},
		cache: false,
		contentType: false,
		processData: false
	});	
}