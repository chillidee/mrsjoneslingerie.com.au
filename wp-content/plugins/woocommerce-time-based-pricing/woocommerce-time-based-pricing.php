<?php 
/*
Plugin Name: WooCommerce Pricing!
Description: Set products prices or discounts per time periods or recurring events according to product quantities and user roles.
Author: Lagudi Domenico
Version: 7.6
*/

/* 
Copyright: WooCommerce Pricing! uses the ACF PRO plugin. ACF PRO files are not to be used or distributed outside of the WooCommerce Pricing! plugin.
*/
define('WCTBP_PLUGIN_PATH', rtrim(plugin_dir_url(__FILE__), "/") )  ;
define('WCTBP_PLUGIN_ABS_PATH', plugin_dir_path( __FILE__ ) );

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) 
{
	include 'classes/com/WCTBP_Acf.php';
	
	$wctbp_cart = array();
	if(!class_exists('WCTBP_Tax'))
	{
		require_once('classes/com/WCTBP_Tax.php');
	}
	if(!class_exists('WCTBP_Wpml'))
	{
		require_once('classes/com/WCTBP_Wpml.php');
		$wctbp_wpml_helper = new WCTBP_Wpml();
	}
	if(!class_exists('WCTBP_Option'))
	{
		require_once('classes/com/WCTBP_Option.php');
		$wctbp_option_model = new WCTBP_Option();
	}
	if(!class_exists('WCTBP_Dashboard'))
	{
		require_once('classes/admin/WCTBP_Dashboard.php');
		$wctbp_dashboard = new WCTBP_Dashboard();
	}
	if(!class_exists('WCTBP_User'))
	{
		require_once('classes/com/WCTBP_User.php');
		$wctbp_user_model = new WCTBP_User();
	}
	if(!class_exists('WCTBP_Time'))
	{
		require_once('classes/com/WCTBP_Time.php');
		$wctbp_time_model = new WCTBP_Time();
	}
	if(!class_exists('WCTBP_Product'))
	{
		require_once('classes/com/WCTBP_Product.php');
		$wctbp_product_model = new WCTBP_Product();
	}
	if(!class_exists('WCTBP_PriceChanger'))
	{
		require_once('classes/frontend/WCTBP_PriceChanger.php');
		$wctbp_price_changer = new WCTBP_PriceChanger();
	}	
	if(!class_exists('WCTBP_ShopPage'))
	{
		require_once('classes/frontend/WCTBP_ShopPage.php');
		$wctbp_shop_page = new WCTBP_ShopPage();
	}
	if(!class_exists('WCTBP_ProductPage'))
	{
		require_once('classes/frontend/WCTBP_ProductPage.php');
		$wctbp_product_page = new WCTBP_ProductPage();
	}
	if(!class_exists('WCTBP_Cart'))
	{
		require_once('classes/com/WCTBP_Cart.php');
		$wctbp_cart_addon = new WCTBP_Cart();
	}
	if(!class_exists('WCTBP_ProductTablePage'))
	{
		require_once('classes/admin/WCTBP_ProductTablePage.php');
		$wctbp_product_table_addon = new WCTBP_ProductTablePage();
	}
	if(!class_exists('WCTBP_TextCustomizerPage'))
	{
		require_once('classes/admin/WCTBP_TextCustomizerPage.php');
		
	}
	if(!class_exists('WCTBP_Text'))
	{
		require_once('classes/com/WCTBP_Text.php');
		$wctbp_text_model = new WCTBP_Text();
	}
	if(!class_exists('WCTBP_PricesConfiguratorPage'))
		require_once('classes/admin/WCTBP_PricesConfiguratorPage.php');
	if(!class_exists('WCTBP_GeneralOptionsPage'))
		require_once('classes/admin/WCTBP_GeneralOptionsPage.php');
	
	
	load_plugin_textdomain('woocommerce-time-based-pricing', false, basename( dirname( __FILE__ ) ) . '/languages' );
	add_action('admin_menu', 'wctbp_init_admin_panel');
	add_action('admin_init', 'wctbp_admin_init');
}
function wctbp_admin_init()
{
	$remove = remove_submenu_page( 'woocommerce-time-based-pricing', 'woocommerce-time-based-pricing');
}	
function wctbp_init_admin_panel()
{
	if(!current_user_can('manage_woocommerce'))
		return;
	$place = $place = wctbp_get_free_menu_position(55, 0.1);
	
	$hookname  = add_menu_page( __( 'WooCommerce Pricing!', 'woocommerce-time-based-pricing' ), __( 'WooCommerce Pricing!', 'woocommerce-time-based-pricing' ), 'manage_woocommerce', 'woocommerce-time-based-pricing', null, WCTBP_PLUGIN_PATH."/images/dollar-icon.png", (string)$place );
	//add_submenu_page('woocommerce-time-based-pricing', __('Configurator','woocommerce-time-based-pricing'), __('Configurator','woocommerce-time-based-pricing'), 'edit_shop_orders', 'woocommerce-time-based-pricing-bulk-import', 'wctbp_render_bulk_import_page');
	$price_configurator = new WCTBP_PricesConfiguratorPage();
	$general_options = new WCTBP_GeneralOptionsPage();
	$wctbp_text_customizer_page = new WCTBP_TextCustomizerPage();
	
	//wctbp_var_dump($remove );	
}
function wctbp_get_free_menu_position($start, $increment = 0.1)
{
	foreach ($GLOBALS['menu'] as $key => $menu) {
		$menus_positions[] = $key;
	}
	
	if (!in_array($start, $menus_positions)) return $start;

	/* the position is already reserved find the closet one */
	while (in_array($start, $menus_positions)) 
	{
		$start += $increment;
	}
	return $start;
}
function wctbp_var_dump($data)
{
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
}
?>