<?php 
class WCTBP_Text
{
	public function __construct()
	{
	}
	public function get_texts()
	{
		$all_data = array();
		$all_data['sale_badge_text'] = get_field('wctbp_sale_badge_text', 'option'); 		
		$all_data['sale_badge_text'] = $all_data['sale_badge_text'] != null ? $all_data['sale_badge_text'] : ""; 		
		
		$all_data['product_page_after_price_text'] = get_field('wctbp_product_page_after_price_text', 'option'); 		
		$all_data['product_page_after_price_text'] = $all_data['product_page_after_price_text'] != null ? $all_data['product_page_after_price_text'] : ""; 		

		return $all_data;
	}
}
?>